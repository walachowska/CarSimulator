package controller;

import view.RaceView;
import model.Race;

public class RaceController
{
    public RaceController()
    {
        RaceView view = new RaceView();
        Race race = new Race(view.getDriverNames());

        race.start();

        view.showWinner(race);
        view.showSeconds(race);
        view.showDistances(race);
    }
}
