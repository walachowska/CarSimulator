package model;

import java.util.ArrayList;

public class Race
{
    public ArrayList<Car> cars;
    public Car winner = null;
    public int seconds = 0;

    public Race(ArrayList<String> driverNames)
    {
        this.cars = new ArrayList<Car>();

        for (String driverName : driverNames) {
            this.cars.add(new Car(driverName));
        }
    }

    public void start()
    {
        int RACE_COURSE = 10000;

        while (this.winner == null) {
            this.seconds++;
            for (Car car : this.cars) {
                car.drive();
                if (car.distanceTravelled >= RACE_COURSE) {
                    this.winner = car;
                }
            }
        }
    }
}
