package view;

import model.Car;
import model.Race;
import java.util.ArrayList;
import java.util.Scanner;

public class RaceView
{
    public ArrayList<String> getDriverNames()
    {
        System.out.print("How many cars are entering the race: ");

        Scanner keyboard = new Scanner(System.in);
        int numberOfDrivers = keyboard.nextInt();
        ArrayList<String> driverNames = new ArrayList<String>();

        for (int i = 0; i < numberOfDrivers; i++) {
            System.out.print("What is the name of the driver: ");
            String driverName = keyboard.next();
            driverNames.add(driverName);
        }

        return driverNames;
    }

    public void showWinner(Race race)
    {
        System.out.println(race.winner.driverName + " won the race!");
    }

    public void showSeconds(Race race)
    {
        System.out.println("The race took " + race.seconds + " seconds");
    }

    public void showDistances(Race race)
    {
        for (Car car : race.cars) {
            System.out.println(car.driverName + " drove " + car.distanceTravelled + " meters");
        }
    }
}
